package com.example.jonatan.testeo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Jonatan on 25/06/2018.
 */

public class ThirdActivity extends AppCompatActivity{
    private EditText etInputEmail;
    private EditText etInputPassword;
    private EditText etInputPasswordRepeat;
    private EditText etInputName;

    @Override
    protected void onCreate(Bundle persistenceBundle){
        super.onCreate(persistenceBundle);
        setContentView(R.layout.third_activity_main);

        findViewById(R.id.btnRegisterNewUser).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    etInputEmail = (EditText) findViewById(R.id.etInputEmail);
                    etInputPassword = (EditText) findViewById(R.id.etInputPassword);
                    etInputPasswordRepeat = (EditText) findViewById(R.id.etInputPasswordRepeat);
                    etInputName = (EditText) findViewById(R.id.etInputName);

                    String inputEmail = etInputEmail.getText().toString();
                    String inputPassword = etInputPassword.getText().toString();
                    String inputPasswordRepeat = etInputPasswordRepeat.getText().toString();
                    String inputName = etInputName.getText().toString();

                    if(inputEmail.isEmpty() || inputPassword.isEmpty() ||
                            inputPasswordRepeat.isEmpty() || inputName.isEmpty()){
                        Toast.makeText(getApplicationContext(), "Complete todos los campos",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!inputPassword.equals(inputPasswordRepeat)) {
                        Toast.makeText(getApplicationContext(), "Las contraseñas no coinciden",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Intent i = new Intent();
                    Usuario usuario = new Usuario(inputName, inputEmail, inputPassword);
                    i.putExtra("usuarioDevueltoPorActividad", usuario);
                    setResult(RESULT_OK, i);
                    finish();
                }catch(Exception e) {
                    Log.i("Tercera actividad", "=====Ver LogCat=====");
                    Toast.makeText(getApplicationContext(), "Excepción. Ver LogCat",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
