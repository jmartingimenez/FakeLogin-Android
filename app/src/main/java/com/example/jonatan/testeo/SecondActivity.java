package com.example.jonatan.testeo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Jonatan on 18/06/2018.
 */

public class SecondActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle persistenceBundle){
        super.onCreate(persistenceBundle);
        setContentView(R.layout.second_activity_main);

        String miMensajeRecuperado = getIntent().getStringExtra("miMensaje");
        Usuario usuario = getIntent().getParcelableExtra("usuario");

        TextView textView = (TextView)findViewById(R.id.saludoTextView);
        textView.setText(miMensajeRecuperado + usuario.getNombre() + "/" +
                usuario.getEmail() + "/" + usuario.getPassword());

        findViewById(R.id.btnShare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, "Algo compartido!!!!!");
                startActivity(i);
            }
        });
    }
}
