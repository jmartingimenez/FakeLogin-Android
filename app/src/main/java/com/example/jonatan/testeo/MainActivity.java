package com.example.jonatan.testeo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText editTextName;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private Usuario usuario;

    @Override
    protected void onActivityResult(int requestCode, int result_code, Intent data){
        super.onActivityResult(requestCode, result_code, data);
        if(requestCode == 237 && result_code == RESULT_OK){
            usuario = (Usuario) data.getParcelableExtra("usuarioDevueltoPorActividad");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); 

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editTextName = (EditText) findViewById(R.id.inputName);
                editTextEmail = (EditText) findViewById(R.id.inputEmail);
                editTextPassword = (EditText) findViewById(R.id.inputPassword);

                String nombreDelUser = editTextName.getText().toString();
                String emailDelUser = editTextEmail.getText().toString();
                String passwordDelUser = editTextPassword.getText().toString();

                Intent i = new Intent(MainActivity.this, SecondActivity.class);
                i.putExtra("miMensaje", "Datos: ");

                if(usuario != null && usuario.getEmail().equals(emailDelUser) &&
                        usuario.getPassword().equals(passwordDelUser)){
                        usuario = new Usuario(nombreDelUser,emailDelUser,passwordDelUser);
                        i.putExtra("usuario", usuario);
                        startActivity(i);
                }else Toast.makeText(getApplicationContext(), "No se encuentra el usuario",
                        Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.btnRegister).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ThirdActivity.class);
                startActivityForResult(i, 237);
            }
        });
    }
}
